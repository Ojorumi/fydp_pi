let recButton;
let recButtonText;

let tealButton;

let yellowButton;

let orangeButton;

let blueButton;

let stopButton;

let titleFont;
let bodyFont;

let currRecording = false;
let bgColorIndex = 0;

function preload() {
  //bodyFont = loadFont("../fonts/Jost-Regular.ttf");
  titleFont = loadFont("../fonts/BungeeShade-Regular.ttf");
  bodyFont = loadFont("../fonts/RobotoMono-Regular.ttf");
  //titleFont = loadFont("../fonts/Rubik-ExtraBold.ttf");
}

function setup() {
	frameRate(20);
	
	// Colours cannot be global: p5js functions (color()) must be inside setup() or draw()
	const darkBlue = color(0, 37, 50);
	const darkTeal = color(0, 66, 59);
	const darkYellow = color(136, 131, 0);
	const darkOrange = color(168, 97, 13);
	const darkRed = color(173, 46, 27);
	
	const blue = color(0, 53, 71);
	const teal = color(0, 94, 84);
	const yellow = color(194, 187, 0);
	const orange = color(237, 139, 22);
	const red = color(225, 82, 61);
	
	const transparent = color(0, 0, 0, 0);
	
	let titleFontSize = windowHeight/12;
	let bodyFontSize = windowHeight/25;
	const smallBodyFontSize = windowHeight/35;
	
	// Dimensions
	const recButtonWidth = Math.min(300, (windowWidth - 50));
	const recButtonHeight = windowHeight/13;
	const buffer = windowHeight/60;
	const recButtonPos = [(windowWidth - recButtonWidth)/2, windowHeight/10 + titleFontSize/2];
	const titleVerticalPos = windowHeight/10;
	const speakerButtonWidth = (recButtonWidth - buffer)/2;
	const speakerButtonHeight = recButtonHeight;
	
	// Positions image
	const posImageWidth = recButtonWidth;
	const posImageHeight = posImageWidth * (480/640);	// original png: 640x480 pixels
	
	createCanvas(windowWidth, windowHeight);

	// Start recording button
	uxFill(blue);
	uxStroke(darkBlue);
	recButton = uxRect(recButtonPos[0], recButtonPos[1], recButtonWidth, recButtonHeight);
	recButton.uxEvent('click', recButtonClick);
	
	const speakerButtonLeft = (windowWidth - recButtonWidth)/2;
	const speakerButtonRight = (windowWidth + recButtonWidth)/2;
	const speakerButtonTop = recButtonPos[1] + 2*buffer + posImageHeight + smallBodyFontSize;
	const speakerButtonBottom = speakerButtonTop + buffer + speakerButtonHeight;
	
	uxFill(transparent);
	uxStroke(transparent);
	tealButton = uxRect(speakerButtonLeft, speakerButtonTop, speakerButtonWidth, speakerButtonHeight);
	tealButton.uxEvent('click', tealButtonClick);
	
	yellowButton = uxRect(speakerButtonRight - speakerButtonWidth, speakerButtonTop, speakerButtonWidth, speakerButtonHeight);
	yellowButton.uxEvent('click', yellowButtonClick);
	
	orangeButton = uxRect(speakerButtonLeft, speakerButtonBottom, speakerButtonWidth, speakerButtonHeight);
	orangeButton.uxEvent('click', orangeButtonClick);
	
	blueButton = uxRect(speakerButtonRight - speakerButtonWidth, speakerButtonBottom, speakerButtonWidth, speakerButtonHeight);
	blueButton.uxEvent('click', blueButtonClick);
	
	// Stop recording button
	const stopButtonHeight = speakerButtonBottom + buffer + speakerButtonHeight + smallBodyFontSize;
	stopButton = uxRect((windowWidth - recButtonWidth)/2, stopButtonHeight, recButtonWidth, recButtonHeight);
	stopButton.uxEvent('click', stopButtonClick);
}

function draw() {
	const darkBlue = color(0, 37, 50);
	const darkTeal = color(17, 51, 48);
	const darkYellow = color(112, 109, 28);
	const darkOrange = color(153, 90, 14);
	const darkRed = color(173, 46, 27);
	
	const blue = color(0, 53, 71);
	const teal = color(0, 94, 84);
	const yellow = color(194, 187, 0);
	const orange = color(237, 139, 22);
	const red = color(225, 82, 61);

	const white = color(255, 255, 255);
	
	const bgGrey = color(240, 240, 240);
	const bgBlue = color(111, 179, 219);
	const bgTeal = color(102, 178, 168);
	const bgYellow = color(240, 239, 131);
	const bgOrange = color(240, 178, 104);
	
	const bgColor = [bgGrey, bgBlue, bgTeal, bgYellow, bgOrange];
	
	// Array to handle updating background color
	const titleFontSize = windowHeight/12;
	const bodyFontSize = windowHeight/25;
	const smallBodyFontSize = windowHeight/35;
	
	// Dimensions
	const recButtonWidth = Math.min(300, (windowWidth - 50));
	const recButtonHeight = windowHeight/13;
	const buffer = windowHeight/60;
	const recButtonPos = [(windowWidth - recButtonWidth)/2, windowHeight/10 + titleFontSize/2];
	const titleVerticalPos = windowHeight/10;
	const speakerButtonWidth = (recButtonWidth - buffer)/2;
	const speakerButtonHeight = recButtonHeight;
	
	// Positions image
	const posImageWidth = recButtonWidth;
	const posImageHeight = posImageWidth * (480/640);	// original png: 640x480 pixels

	background(bgColor[bgColorIndex]);
	
	fill(blue);
	stroke(darkBlue);
	textFont(titleFont);
	textAlign(CENTER);
	textSize(titleFontSize);
	let title = text('LISTENX', windowWidth/2, titleVerticalPos);
	
	if (currRecording === false) {
		recButton.uxRender();
		
		fill(white);
		stroke(white);
		textFont(bodyFont);
		textSize(bodyFontSize);
		recButtonText = text('Start Recording', windowWidth/2, titleVerticalPos + titleFontSize + recButtonHeight/2.5 - bodyFontSize/2);
	}
	else {
		stopButton.uxRender();
		stopButton.uxFill = red;
		stopButton.uxStrokeColor = darkRed;

		tealButton.uxRender();
		tealButton.uxFill = teal;
		tealButton.uxStrokeColor = darkTeal;
		
		yellowButton.uxRender();
		yellowButton.uxFill = yellow;
		yellowButton.uxStrokeColor = darkYellow;
		
		orangeButton.uxRender();
		orangeButton.uxFill = orange;
		orangeButton.uxStrokeColor = darkOrange;
		
		blueButton.uxRender();
		blueButton.uxFill = blue;
		blueButton.uxStrokeColor = darkBlue;
		
		fill(white);
		stroke(white);
		textFont(bodyFont);
		textSize(bodyFontSize);
		const stopButtonTextHeight = recButtonPos[1] + posImageHeight + 2*speakerButtonHeight + recButtonHeight/2.5 + bodyFontSize/2 + 6*buffer + smallBodyFontSize;
		stopButtonText = text('Stop Recording', windowWidth/2, stopButtonTextHeight);
		
		textAlign(LEFT)
		textSize(smallBodyFontSize);
		fill(blue);
		stroke(blue);
		const chooseSpeakerTextHeight = recButtonPos[1] + 2*buffer + posImageHeight + smallBodyFontSize/2;
		choseSpeakerText = text('Choose Speaker:', (windowWidth - recButtonWidth)/2, chooseSpeakerTextHeight);
	}
}

function recButtonClick() {
	currRecording = !currRecording;
	bgColorIndex = 0;
}

function stopButtonClick() {
	currRecording = !currRecording;
	bgColorIndex = 0;
}

function tealButtonClick() {
	bgColorIndex = 2;
}

function yellowButtonClick() {
	bgColorIndex = 3;
}

function orangeButtonClick() {
	bgColorIndex = 4;
}

function blueButtonClick() {
	bgColorIndex = 1;
}

//best_color = color(135,62,35);

#required imports
from flask import Flask, request, send_file, jsonify

import sys
import os
import time
import numpy as np

from queue import Queue
from threading import Thread

import pyaudio
import librosa
import wave

from asteroid.models import ConvTasNet

sys.path.append("/home/kunalchandan/capstone/fydp_pi/fydp_pi/fast/server/Cone-of-Silence")
import inference

#setting path to write input data to
input_data_path = "/home/kunalchandan/capstone/fydp_pi/fydp_pi/fast/server/processed/"

#initialize app
app = Flask(__name__)

#global variables

#global queue to store input files
g_queue = Queue()

#global array to store output files
red     = [0, 0, 0, 0, 0]
blue    = [0, 0, 0, 0, 0]
orange  = [0, 0, 0, 0, 0]
green   = [0, 0, 0, 0, 0]

#global integer to track index of output file arrays
write_index = 0
read_index = 0

#global variable to specicify which speaker to send
speaker = None

#function to save the incoming audio data into a wav file
def save_audio_data(fname, channels, format, sample_rate, frames):
    p = pyaudio.PyAudio()
    wf = wave.open(fname, 'wb')
    wf.setnchannels(channels)
    wf.setsampwidth(p.get_sample_size(format))
    wf.setframerate(sample_rate)
    wf.writeframes(b''.join(frames))
    wf.close()

#function to run convtasnet on specified audio file
def run_convtasnet(filename):
    model = ConvTasNet.from_pretrained("mhu-coder/ConvTasNet_Libri1Mix_enhsingle")
    model.separate(filename, resample=True, force_overwrite=True)



#function to run inference on audio input
def worker():

    #get global variables for output files and index
    global red, blue, orange, green
    global write_index

    #infinite loop - should always be running inference on latest audio input
    while True:

        # get latest audio input from queue
        input_file_name = g_queue.get()

        # run inference on audio input
        output_files = inference.main(input_file_name)

        #assign output files to respective output array
        red[write_index]      = output_files[0]
        blue[write_index]     = output_files[1]
        orange[write_index]   = output_files[2]
        green[write_index]    = output_files[3]

        #incrementing the write_index counter (must wrap around)
        write_index = (write_index + 1)%5

        os.remove(input_file_name)

        #enqueued task is complete - other threads can now access the queue
        g_queue.task_done()

def get_speaker():
    global speaker
    while(True):
        time.sleep(1)
        print(speaker)


@app.route("/send_audio", methods=["GET"])
def send_audio_file():
    global red, blue, orange, green
    global read_index
    #image_path = "/home/kunalchandan/capstone/fydp_pi/fydp_pi/fast/server/processed/processed_image/positions.png"

    app_path = "/home/kunalchandan/capstone/fydp_pi/fydp_pi/fast/server/processed"
        
    #creating a variable that stores required file path
    file_to_send = 0

    #get required file path based on which speaker is selected
    if speaker == "red":
        file_to_send = red[read_index]

    elif speaker == "blue":
        file_to_send = blue[read_index]

    elif speaker == "orange":
        file_to_send = orange[read_index]

    elif speaker == "green":
        file_to_send = green[read_index]
    
    #check if file to send has valid path --> if so send it
    if file_to_send != 0:
        file_to_send = os.path.join(app_path, file_to_send)
        read_index = (read_index + 1)%5
        print("read index = " + str(read_index) + " and the filename = " + file_to_send)
        return send_file(file_to_send, mimetype="audio/wav")
    
    else:
        return "waiting for audio to be processed"
        


# #function to return speaker position png to app.
@app.route("/get_speaker_position")
def get_speaker_pos_png():
    image_path = "/home/kunalchandan/capstone/fydp_pi/fydp_pi/fast/server/processed/000/positions.png"
    if os.path.exists(image_path):
        return send_file(image_path)
    else:
        return 404

#function to return a selected speaker based on colour
@app.route("/get_red_speaker")
def set_red_speaker_audio():
    global speaker
    speaker = "red"
    return "getting red"

@app.route("/get_blue_speaker")
def set_blue_speaker_audio():
    global speaker
    speaker = "blue"
    return "getting blue"

@app.route("/get_orange_speaker")
def set_orange_speaker_audio():
    global speaker
    speaker = "orange"
    return "getting orange"

@app.route("/get_green_speaker")
def set_green_speaker_audio():
    global speaker
    speaker = "green"
    return "getting green"

#function to process and store incoming audio stream in queue
@app.route('/processChunk', methods=['POST'])
def receive_chunk():
    num_channels = 5

    # Reshape PyAudio ByteStream into numpy array for COS
    audio_chunk = np.fromstring(request.data, dtype=np.int32)
    chunk_length = len(audio_chunk)//num_channels
    assert chunk_length == int(chunk_length)
    audio_chunk = np.reshape(audio_chunk, (num_channels, chunk_length))
    

    # Save AUDIO DATA takes 3ms to run
    input_file_name = f"{input_data_path}/input_stream/{int(time.time())%1000}.wav"
    save_audio_data(fname=input_file_name, 
                    channels=num_channels, 
                    format=pyaudio.paInt32, 
                    sample_rate=16000, 
                    frames=audio_chunk)
    
    #placing audio input into queue
    g_queue.put(input_file_name)

    #debugging statements
    #print(f"Length of global queue: {g_queue.qsize()}")
    #print(audio_chunk.shape)

    #required to complete post request
    return f'Audio chunk received {int(time.time())}'
    

#function to run server
def run_flask_server():
    app.run(host='0.0.0.0', port=8080, ssl_context='adhoc')


if __name__ == "__main__":

    #initializing model - required for model to load into cache, etc
    # inference.main(np.zeros((4, 128)))

    #create thread to run flask app
    server_thread = Thread(target=run_flask_server)
    server_thread.start()

    #create thread to run inference worker
    inference_thread = Thread(target=worker)
    inference_thread.start()

    #create thread to send audio to client
    send_audio_thread = Thread(target=get_speaker)
    send_audio_thread.start()
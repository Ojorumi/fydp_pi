from flask import Flask, request
import subprocess

import threading

import os
import sys
import argparse  # Argment parser

from collections import deque 
import numpy as np

import time
import librosa  # Music and audio analysis
import wave  # Read wav files

import pyaudio
import sounddevice as sd
import requests 
app = Flask(__name__)
frames = []
recording_now = False


def send_wav(filename):
    with open(filename, 'rb') as f:
        try:
            response = requests.post(url="http://192.168.2.12:8080/processWAV", 
                    auth=("fydp", "kierstenoverton2000$"),  
                    files={"file": f}, verify=False)
            return response
        except TimeoutError:
            print("Connection timed out!")
            return response
        else:
            print(response)
            return response


def stream_chunks():
    # when started, starts writing audio data to np array. 
    # when stopped writes to outputClip.wav in the recordings directory

    CHUNK = 1024
    FORMAT = pyaudio.paInt32
    CHANNELS = 5
    RATE = 16000
    WAVE_OUTPUT_FILENAME = "recordings/recording.wav"
    RECORD_SECONDS = 30

    p = pyaudio.PyAudio()

    stream = p.open(format=FORMAT,
                    channels=CHANNELS,
                    rate=RATE,
                    input=True,
                    frames_per_buffer=CHUNK)

    print("Recording started.")
    global frames

    while True:
    #for i in range(0, int(RATE/CHUNK * RECORD_SECONDS)):
        if not recording_now:
            break
        data = stream.read(CHUNK)
        frames.append(data)
    stream.stop_stream()
    stream.close()
    p.terminate()
    print("Recording finished.")
    
    print(f"Writing to {WAVE_OUTPUT_FILENAME}.")
    wf = wave.open(WAVE_OUTPUT_FILENAME, 'wb')
    wf.setnchannels(CHANNELS)
    wf.setsampwidth(p.get_sample_size(FORMAT))
    wf.setframerate(RATE)
    wf.writeframes(b''.join(frames))
    wf.close()
    print(f"Finished writing to {WAVE_OUTPUT_FILENAME}.")


@app.route("/startRecording", methods=["GET"])
def start_recording():
    global recording_now
    recording_now = True
    thread = threading.Thread(target=stream_chunks)
    thread.start()
    return "Streaming started.", 200


@app.route("/stopRecording", methods=["GET"])
def stop_recording():
    global recording_now
    if recording_now:
        recording_now = False
    time.sleep(0.5)
    response = send_wav('/home/pi/fydp/slow/recordings/recording.wav')
    return "Streaming stopped", 200


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000)

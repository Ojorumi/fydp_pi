import os
import sys

import argparse

from collections import deque
import numpy as np

import time

import librosa
import wave 

import pyaudio
import sounddevice as sd
from asteroid.models import ConvTasNet

sys.path.append("/Users/fydp/Gitlab/fydp_pi/code/slow/server/Cone-of-Silence/cos/inference/")
import separation_by_localization

def main(filename):
    #run inference
    #concatenate into multiple channels
    
    def concat_wav(infiles, outfile):
        data= []
        for infile in infiles:
            w = wave.open(infile, 'rb')
            data.append( [w.getparams(), w.readframes(w.getnframes())] )
            w.close()
    
        output = wave.open(outfile, 'wb')
        output.setparams(data[0][0])
        for i in range(len(data)):
            output.writeframes(data[i][1])
        output.close()
    
    channels   = 4
    fs         = 44100
    radius     = .03231
    model_path  = "/Users/fydp/Gitlab/fydp_pi/code/slow/server/Cone-of-Silence/checkpoints/realdata_4mics_.03231m_44100kHz.pt" 
    output_path = "/Users/fydp/Gitlab/fydp_pi/code/slow/server/processed/"
    input_path = "/Users/fydp/Gitlab/fydp_pi/code/slow/server/recordings/recording.wav"
    
    #creating argument class object
    args = argparse.Namespace(debug=False, 
                                duration=18,
                                input_file=input_path, 
                                mic_radius=radius, 
                                model_checkpoint=model_path, 
                                moving=False, 
                                n_channels=channels, 
                                output_dir=output_path, 
                                sr=fs, 
                                use_cuda=False
                                )
    
    separation_by_localization.main(args)

    # TODO: concatenate into 4 audio channels


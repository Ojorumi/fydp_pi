from flask import Flask, request
import subprocess

import asyncio

import os
import sys
import argparse  # Argment parser

from collections import deque
import numpy as np

import time
import librosa  # Music and audio analysis
import wave  # Read wav files

import pyaudio
import sounddevice as sd

sys.path.append("/Users/fydp/Gitlab/fydp_pi/code/slow/server/Cone-of-Silence/")
import inference

app = Flask(__name__)
frames = []
UPLOAD_DIRECTORY = "/Users/fydp/Gitlab/fydp_pi/slow/server/"
ALLOWED_EXTENSIONS = ".wav"

async def stream_chunks():
    pass

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/processWAV', methods=['POST'])
def receive_file():
    file = request.files['file']

    # check if the post request has the file part
    if 'file' not in request.files:
        return 'No file part'

    # if user does not select file, browser also
    # submit an empty part without filename
    if file.filename == '':
        return 'No selected file'

    if file and allowed_file(file.filename):
        filename = file.filename
        file.save(os.path.join(UPLOAD_DIRECTORY, filename))
        inference.main(filename)
        return 'File Received'

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=6000)



